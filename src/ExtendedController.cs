﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace RoutingExtensions
{
	public abstract class ExtendedController : Controller
	{
		public ActionResult RedirectTo<T>(Expression<Func<T, ActionResult>> expression)
			where T : Controller
		{
			var routeInfo = expression.ExtractRouteInfo();

			return this.RedirectToAction(
				actionName: routeInfo.ActionName,
				controllerName: routeInfo.ControllerName,
				routeValues: routeInfo.RouteValues.ToRouteValueDictionary());
		}

		public ActionResult RedirectToReferer()
			=> this.Redirect(this.Request.UrlReferrer.ToString());
	}
}
